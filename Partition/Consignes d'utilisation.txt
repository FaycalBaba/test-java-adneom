

 * Deux versions; une méthode récursive et une méthode itérative (méthodes génériques).
 * Ajouter la librairie Partition.jar.
 * Importer la class "import adneom.code.Partition", on peut aussi importer statiquement.
 * les deux méthodes prennent respectivement la liste comme premier paramètre et la taille des partitions pour deuxième paramètres.
 * Les méthodes lancent une IllegalParametersException si les paramètres ne sont pas conformes (liste null, taille null / taille<=0 ).
 * Exemple : 
	List<String> l = Arrays.asList("1","2","3","4","5","6");
	System.out.println(Partition.partitionning_iterative(l, 3));
	console: 
 		[[1, 2, 3], [4, 5, 6]]

 * Pour les tests JUnit: utilisation d'une librairie ajouter via Maven permettant d'effectuer des Tests sur les Lists
 
 
 	