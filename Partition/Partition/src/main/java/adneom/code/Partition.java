package adneom.code;

import java.util.ArrayList;
import java.util.List;

public final class Partition {
	
			
	  public static final <T> List<List<T>> partitionning_recursive(List<T> l, Integer n) throws IllegalArgumentException{
		  
		  if(l == null || n == null || n<=0) {
			  throw new IllegalArgumentException();
		  }
		  ArrayList<List<T>> subList = new ArrayList<List<T>>();
		  
		  if(n>l.size() || n <=0) {
			  
			  if(!l.isEmpty()) {
				  subList.add(l);				  
			  }
			  return subList;
		  }
		  
		  subList.add(l.subList(0, n));
		  
		  List<List<T>> returned = partitionning_recursive(l.subList(n, l.size()), n);
		  
		  if(returned.isEmpty())  {
			  return subList;
		  }
		  subList.addAll(returned);
		  return subList;
	  }
	  
	  
	  
	  public static final <T> List<List<T>> partitionning_iterative(List<T> l, Integer n) throws IllegalArgumentException{
		  
		  if(l == null || n == null || n<=0) {
			  throw new IllegalArgumentException();
		  }

		  ArrayList<List<T>> partitionedList = new ArrayList<List<T>>();
		  while(l.size()>=n) {
			  partitionedList.add(l.subList(0, n));
			  l = l.subList(n, l.size());
		  }
		  
		  if(!l.isEmpty()) {
			partitionedList.add(l);  
		  }
		  
		return partitionedList;
	  }
	  
}