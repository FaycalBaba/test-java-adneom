package adneom.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import adneom.code.Partition;

public class PartitionTest {

	List<List<String>> TestListExpected = new ArrayList<List<String>>();

	List<String> normalTestList = Arrays.asList("aa", "bb", "cc", "dd", "ee", "ff");

	List<String> sublist1 = Arrays.asList("aa", "bb", "cc");
	List<String> sublist2 = Arrays.asList("dd", "ee", "ff");

	List<String> sublist3 = Arrays.asList("aa", "bb", "cc","dd", "ee", "ff");
	
	
	@Test
	public void testPartitionIterative() {
		//Correct expected use of method
		List<List<String>> testListActual = Partition.partitionning_iterative(normalTestList, 3);
		
		TestListExpected.add(sublist1);TestListExpected.add(sublist2);
		// [["aa","bb","cc"],["dd","ee","ff"]]
		
		
		assertThat(testListActual, is(TestListExpected));

	}
	
	@Test
	public void testPartitionIterative1() {
		// Test with n>list.size()
		List<List<String>> testListActual = Partition.partitionning_iterative(normalTestList, 9);
		
		
		TestListExpected.add(sublist3);
		// [["aa","bb","cc","dd","ee","ff"]]
		
		
		assertThat(testListActual, is(TestListExpected));

	}
	
	@Test
	public void testPartitionIterative2() {
		// Test with n>list.size()
		
		try {
			List<List<String>> testListActual = Partition.partitionning_iterative(normalTestList, null);
			assertEquals(testListActual, null);
		}catch(IllegalArgumentException e) {
			System.err.println("Method returned null");
		}
		
		// null
		
		
		
	}
	
	

}
